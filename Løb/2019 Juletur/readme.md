# PostNord Terminal

## Description

PowerShell script used to simulate a PC Terminal with password login, for Juletur's natteløb.

## Usage

Run the PostNord.ps1 script with PowerShell.

### Progam Title

The Title of the console windows is defined by the $Title variable.

### Password

The password for the script is defined by the $ScriptPassword variable.
